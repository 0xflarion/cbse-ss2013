package entities;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import javax.persistence.ManyToOne;

import enumerations.EAllocationType;

/**
 * Entity class of appointment.
 */
@Entity
@Table(name = "APPOINTMENT")
public class Appointment implements Serializable, Comparable<Appointment> {

	private static final long serialVersionUID = -1978140455833711394L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	//@JoinColumn(name = "userName")
	private UserAccount userAccount;
	
	private String title;
	
	@Column(nullable = false) 
	private boolean isPrivate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	@Column(columnDefinition = "text", nullable = false)
	private String notes;
	
	@Enumerated(EnumType.STRING)
	private EAllocationType allocationType;

	@Column
	private int partnerAppointmentId;	
	
	
	public int getId() { return id; }
	public UserAccount getUserAccount() { return userAccount; } 
	public String getTitle() { return title; }
	public boolean isPrivate() { return isPrivate; }
	public Date getStartDate() { return startDate; }
	public Date getEndDate() { return endDate; }
	public String getNotes() { return notes; }
	public EAllocationType getType() { return allocationType; }
	public int getPartnerAppointmentId() { return partnerAppointmentId; }
	
	public void setId(int appointmentId) { this.id = appointmentId; }
	public void setUserAccount(UserAccount userAccount) { this.userAccount = userAccount; }
	public void setTitle(String title) { this.title = title; }
	public void setPrivate(boolean isPrivate) { this.isPrivate = isPrivate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	public void setNotes(String notes) { this.notes = notes; }
	public void setType(EAllocationType type) { this.allocationType = type; }
	public void setPartnerAppointmentId(int appointmentId) { this.partnerAppointmentId = appointmentId; }
	
	
	
	/**
	 * Appointment with earlier start date is subordinated.<br>
	 * If both start dates are equal, the appointment with earlier end date is subordinated.<br>
	 * Both appointments are coordinate, if both end dates are equal, too.
	 */
	public int compareTo(Appointment o) {
		
		if (this.startDate.getTime() < o.startDate.getTime())
			return -1;
		else if (this.startDate.getTime() > o.startDate.getTime())
			return 1;
		else {
			if (this.endDate.getTime() < o.endDate.getTime())
				return -1;
			if (this.endDate.getTime() > o.endDate.getTime())
				return 1;
			else {
				return 0;
			}
		}
	}
}
