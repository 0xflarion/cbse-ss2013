package entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity class of user account.
 * @author Martin
 *
 */
@Entity
@Table(name = "USERACCOUNT")
public class UserAccount implements Serializable {
	
	private static final long serialVersionUID = 3246949185489421062L;
	
	@Id
	private String userName;
	
	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = false)
	private String email;
	
	@Column(columnDefinition="text", nullable=false)
	private ArrayList<String> notifications = new ArrayList<String>();
	
	
	
	public String getUserName() { return userName; }
	public String getFirstName() { return firstName; }
	public String getLastName() { return lastName; }
	public String getPassword() { return password; }
	public String getEmail() { return email; }
	public ArrayList<String> getNotifications() { return notifications; }
	
	public void setUserName(String userName) { this.userName = userName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	public void setPassword(String password) { this.password = password; }
	public void setEmail(String email) { this.email = email; }
	public void setNotifications(ArrayList<String> notes) { this.notifications = notes; }
}
