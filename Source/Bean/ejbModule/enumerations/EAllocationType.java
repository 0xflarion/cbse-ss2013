package enumerations;

import java.io.Serializable;

/**
 * Enumeration of possible allocation types of an appointment.
 */
public enum EAllocationType implements Serializable {

	/**
	 * Appointment is of type "Free".
	 */
	Free, 
	
	/**
	 * Appointment is of type "Away".
	 */
	Away, 
	
	/**
	 * Appointment is of type "Blocked".
	 */
	Blocked, 
	
	/**
	 * Appointment is of type "Potentially Blocked".
	 */
	Potentially_Blocked

	{
		public String toString() {
			return "Potentially Blocked";
		}
	}
};