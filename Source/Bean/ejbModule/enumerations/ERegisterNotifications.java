package enumerations;

/**
 * Enumeration of registration notifications.
 */
public enum ERegisterNotifications {
	
	/**
	 * Variable userName is null or its length is less than 6 characters.
	 */
	INVALID_USERNAME ("Please enter a username of at least 6 chars length!"),
	
	/**
	 * Variable firstName is null or an empty string.
	 */
	INVALID_FIRSTNAME ("Please enter your first name!"),
	
	/**
	 * Variable lastName is null or an empty string.
	 */
	INVALID_LASTNAME ("Please enter your last name!"),
	
	/**
	 * Variable emailAddress is null, an empty string or does not contain the '@' symbol
	 */
	INVALID_EMAIL ("Please enter a valid email address!"),
	
	/**
	 * Variable password is null, an empty string or its length is less than 6 characters
	 */
	INVALID_PASSWORD ("Please enter a password of at least 6 chars length!"),
	
	/**
	 * User with userName already registered.
	 */
	CONFLICT_USERNAME ("User name is already in use!<br>" +
			"Please select another one."),
	
	/**
	 * User with Email address already registered.
	 */
	CONFLICT_EMAIL ("User with this email address already registered!<br> + " +
			"Please select another one."),
	
	/**
	 * Password and password repetition don't match.
	 */
	PASSWORD_MISMATCH ("Password does not match password repetition!<br>" +
			"Please try again."),
	
	/**
	 * Registration was successful
	 */
	REGISTRATION_SUCCESS ("Registration successful!");
	

	
	private final String name;
	
	private ERegisterNotifications (String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}