package user;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.UserAccount;
import enumerations.ERegisterNotifications;



/**
 * Implementation of IUserManagementBean
 */
@Stateful
@LocalBean
public class UserManagementBean implements IUserManagementBean {
	
	@PersistenceContext
	private EntityManager entityManager;

	private UserAccount currentUser;

	
	/**
	 * Register an account with given data.
	 * @param userName Username of the account to register.
	 * @param firstName First name of user.
	 * @param lastName Last name of user.
	 * @param password Chosen password.
	 * @param passwordRepeat Password repetition. Has to match the parameter "password".
	 * @param email Email address of user.
	 * @return A notification about the result of registration.
	 */
	@SuppressWarnings("unchecked")
	public ERegisterNotifications register(String userName, String firstName, 
			String lastName, String password, String passwordRepeat, String email) {
		
		if (lastName == null || lastName.length() < 1) {
			return ERegisterNotifications.INVALID_LASTNAME;
		}
		if (firstName == null || firstName.length() < 1){
			return ERegisterNotifications.INVALID_FIRSTNAME;
		}
		if (userName == null || userName.length() < 6) {
			return ERegisterNotifications.INVALID_USERNAME;
		}
		if (email == null || email.length() < 1 || !email.contains("@")) {
			return ERegisterNotifications.INVALID_EMAIL;
		}
		if (password == null || password.length() < 6) {
			return ERegisterNotifications.INVALID_PASSWORD;
		}

		Query query = entityManager.createQuery("from UserAccount u where u.userName = :userName");
		query.setParameter("userName", userName);
		List <UserAccount> resultList = query.getResultList();

		if (resultList != null) {
			if (resultList.size() > 0) {
				return ERegisterNotifications.CONFLICT_USERNAME;
			}
		}

		
		query = entityManager.createQuery("from UserAccount u where u.email = :email");
		query.setParameter("email", email);
		resultList = query.getResultList();

		if (resultList != null) {
			if (resultList.size() > 0) {
				return ERegisterNotifications.CONFLICT_EMAIL;
			}
		}

		if (passwordRepeat == null || !password.equals(passwordRepeat)) {
			return ERegisterNotifications.PASSWORD_MISMATCH;
		}

//		Create new user account
		currentUser = new UserAccount();
		currentUser.setLastName(lastName);
		currentUser.setFirstName(firstName);
		currentUser.setUserName(userName);
		currentUser.setEmail(email);
		currentUser.setPassword(passwordRepeat);
		entityManager.persist(currentUser);
		
		return ERegisterNotifications.REGISTRATION_SUCCESS;
	}

	
	/**
	 * Tries to login the user with username/password combination.
	 * @param userName Username of user.
	 * @param password Password of user.
	 * @return <i>account of the logged in user</i> - if login was successful.<br>
	 * 		   <i>null</i> - else
	 */
	@SuppressWarnings("unchecked")
	public UserAccount login(String userName, String password) {
		Query query = entityManager.createQuery("from UserAccount u where u.userName = :userName and u.password = :password");
		query.setParameter("userName", userName);
		query.setParameter("password", password);
		List<UserAccount> resultList = query.getResultList();

		if (resultList == null || resultList.size() <= 0) {
			return null;
		}
		
		this.currentUser = resultList.get(0);
		return this.currentUser;
	}

	
	/**
     * Current user will be logged out.
     */
	public void logout() {
		this.currentUser = null;
	}
	
	
	/**
	 * Returns the currently logged in user.
	 * @return <i>The user currently logged in</i> - if existent<br>
	 * 		   <i>null</i> - else
	 */
    public UserAccount getCurrentUser() {
		return this.currentUser;
	}
}
