package user;
import javax.ejb.Local;

import entities.UserAccount;
import enumerations.ERegisterNotifications;

/**
 * Interface for user management.
 */
@Local
public interface IUserManagementBean {
	
	/**
	 * Register an account with given data.
	 * @param userName Username of the account to register.
	 * @param firstName First name of user.
	 * @param lastName Last name of user.
	 * @param password Chosen password.
	 * @param passwordRepeat Password repetition. Has to match the parameter "password".
	 * @param email Email address of user.
	 * @return A notification about the result of registration.
	 */
	public ERegisterNotifications register (String userName, String firstName, 
			String lastName, String password, String passwordRepeat, String email);
	
	
	/**
	 * Tries to login the user with username/password combination.
	 * @param userName Username of user.
	 * @param password Password of user.
	 * @return <i>account of the logged in user</i> - if login was successful.<br>
	 * 		   <i>null</i> - else
	 */
	public UserAccount login (String userName, String password);
	
	
    /**
     * Current user will be logged out.
     */
	public void logout ();
	
	
	/**
	 * Returns the currently logged in user.
	 * @return <i>The user currently logged in</i> - if existent<br>
	 * 		   <i>null</i> - else
	 */
	public UserAccount getCurrentUser ();
}