package core;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import entities.Appointment;
import entities.UserAccount;
import enumerations.EAllocationType;
import enumerations.ERegisterNotifications;


/**
 * Facade interface to access inner interfaces.
 */
@Local
public interface ICoreManagementBean {
	
	/**
	 * Register an account with given data.
	 * @param userName Username of the account to register.
	 * @param firstName First name of user.
	 * @param lastName Last name of user.
	 * @param password Chosen password.
	 * @param passwordRepeat Password repetition. Has to match the parameter "password".
	 * @param email - Email address of user.
	 * @return A notification about the result of registration.
	 */
	public ERegisterNotifications register(String userName, String firstName, 
			String lastName, String password, String passwordRepeat, String email);

	
	/**
	 * Tries to login the user with username/password combination.
	 * @param userName Username of user.
	 * @param password Password of user.
	 * @return <i>account of the logged in user</i> - if login was successful.<br>
	 * 		   <i>null</i> - else
	 */
	public UserAccount login(String userName, String password);
	
	
    /**
     * Current user will be logged out.
     */
	public void logout();
	
	
	/**
	 * Returns the currently logged in user.
	 * @return <i>The user currently logged in</i> - if existent<br>
	 * 		   <i>null</i> - else
	 */
	public UserAccount getCurrentUser();

	
	/**
	 * Creates an appointment with the given parameters.
	 * @param userName Owner of the appointment.
	 * @param partnerUserName Username of user, the appointment will be shared with. 
	 * <i>null</i> or empty String for no partner.
	 * @param title Title of the appointment.
	 * @param isPrivate <i>true</i> if the appointment should be private.
	 * @param startDate Start date of the appointment.
	 * @param endDate End date of the appointment.
	 * @param allocationType AllocationType of the appointment.
	 * @param notes Additional notes for the appointment.
	 * @return The appointment - if it can be created correctly<br>
	 * 		   <i>null</i> - else
	 */
	public Appointment addAppointment(String userName, String partnerUserName, String title, 
			boolean isPrivate, Date startDate, Date endDate, EAllocationType allocationType, String notes);
	
	
	/**
	 * Deletes the appointment with given ID.
	 * @param appointmentId ID of the appointment you with to delete.
	 */
	public boolean deleteAppointment(int appointmentId);
	
	
	/**
	 * Method to get all appointments of given user within time from start date to end date.
	 * If isOwner is set to <i>false</i>, the list will only contain non-private appointments of the given user.
	 * @param userName Username of the user, the appointment list will be created for.
	 * @param isOwner <i>true</i> if the list should only contain appointments, the user is owner of.
	 * @param startDate Start date of the appointments.
	 * @param endDate End date of the appointments.
	 * @return A list of appointments - if given data is valid<br>
	 * 		   <i>null</i> - else
	 */
	public List<Appointment> getAppointments(String userName, boolean isOwner, Date startDate, Date endDate);
	
	
	/**
	 * Method to return all enum types of EAllocationType.
	 * @return List of allocation types.
	 */
	public List<EAllocationType> getAllocationTypes();
	
	
	/**
	 * Adds a notification to the account of the given user.
	 * @param userName Notification will be added to this user.
	 * @param notification The notification to add.
	 */
	public boolean addNotification(String userName, String notification);
	
	
	/**
	 * Clears the list of notifications for given user.
	 * @param userName Notification list will be cleared for this user.
	 */
	public boolean clearNotifications(String userName);
	
	
	/**
	 * Method returns a global access to PersistenceContext.
	 * @return The entity manger for persistence operations.
	 */
	public EntityManager getEntityManager();
}
