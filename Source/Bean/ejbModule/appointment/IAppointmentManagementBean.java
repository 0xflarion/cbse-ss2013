package appointment;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import entities.Appointment;
import enumerations.EAllocationType;

/**
 * Interface for appointment management.
 */
@Local
public interface IAppointmentManagementBean {
	
	
	/**
	 * Saves the appointment with given ID to users calendar.
	 * @param appointmentId ID of the appointment.
	 * @param userName Owner of the appointment.
	 * @return <i>true</i>  - if appointment saved successfully<br>
	 * 		   <i>false</i> - else
	 */
	public boolean saveAppointment(int appointmentId, String userName);
	
	
	/**
	 * Deletes the appointment with given ID.
	 * @param appointmentId ID of the appointment you with to delete.
	 * @return <i>true</i>  - if appointment saved successfully<br>
	 * 		   <i>false</i> - else
	 */
	public boolean deleteAppointment(int appointmentId);
	
	
	/**
	 * Method to get all appointments of given user within time from start date to end date.
	 * If isOwner is set to <i>false</i>, the list will only contain non-private appointments of the given user.
	 * @param userName Username of the user, the appointment list will be created for.
	 * @param isOwner <i>true</i> if the list should only contain appointments, the user is owner of.
	 * @param startDate Start date of the appointments.
	 * @param endDate End date of the appointments.
	 * @return A list of appointments - if given data is valid<br>
	 * 		   <i>null</i> - else
	 */
	public List<Appointment> getAppointments(String userName, boolean isOwner, Date startDate, Date endDate);
	
	
	/**
	 * Checks if appointment with given start date, end date and allocation type would conflict 
	 * with another appointment of current users calendar.
	 * @param userName Username of user, the check for conflicts should happen for.
	 * @param startDate Start date of the appointment to check.
	 * @param endDate End date of the appointment to check.
	 * @param allocationType Allocation type of the appointment to check.
	 * @param currentUser Currently logged in user.
	 * @return <i>true</i>  - if appointment with given data is in conflict with another appointment<br>
	 * 		   <i>false</i> - else
	 */
	public boolean isAppointmentInConflict (String userName, Date startDate, Date endDate, 
			EAllocationType allocationType, String currentUser);
}
