package appointment;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import entities.Appointment;
import entities.UserAccount;
import enumerations.EAllocationType;


/**
 * Implementation of IAppointmentManagementBean
 */
@Stateful
@LocalBean
public class AppointmentManagementBean implements IAppointmentManagementBean {

	@PersistenceContext
	private EntityManager entityManager;

	
	/**
	 * Saves the appointment with given ID to users calendar.
	 * @param appointmentId ID of the appointment.
	 * @param userName Owner of the appointment.
	 * @return <i>true</i>  - if appointment saved successfully<br>
	 * 		   <i>false</i> - else
	 */
	public boolean saveAppointment(int appointmentId, String userName) {
		Appointment appointment = entityManager.find(Appointment.class, appointmentId);
		if (appointment == null) {
			return false;
		}
		
		UserAccount userAccount = entityManager.find(UserAccount.class, userName);
		if (userAccount == null) {
			return false;
		}

		appointment.setUserAccount(userAccount);
		
		entityManager.persist(appointment);
		return true;
	}
	
	
	/**
	 * Deletes the appointment with given ID.
	 * @param appointmentId ID of the appointment you wish to delete.
	 * @return <i>true</i>  - if appointment deleted successfully<br>
	 * 		   <i>false</i> - else
	 */
	public boolean deleteAppointment(int appointmentId) {
		Appointment appointment = entityManager.find(Appointment.class, appointmentId);
		if (appointment == null) {
			return false;
		}

		entityManager.remove(appointment);
		return true;
	}
	
	
	/**
	 * Method to get all appointments of given user within time from start date to end date.
	 * If isOwner is set to <i>false</i>, the list will only contain non-private appointments of the given user.
	 * @param userName Username of the user, the appointment list will be created for.
	 * @param isOwner <i>true</i> if the list should only contain appointments, the user is owner of.
	 * @param startDate Start date of the appointments.
	 * @param endDate End date of the appointments.
	 * @return A list of appointments - if given data is valid<br>
	 * 		   <i>null</i> - else
	 */
	@SuppressWarnings("unchecked")
	public List<Appointment> getAppointments(String userName, boolean isOwner, Date startDate, Date endDate) {

		if (userName == null || userName.equals("") || startDate == null || endDate == null) {
			return null;
		}

		UserAccount userAccount = entityManager.find(UserAccount.class, userName);

		String queryText = "from Appointment a " + "where a.userAccount = :userAccount and " + "not ((a.startDate > :startDate and a.startDate > :endDate) or (a.endDate < :startDate and a.endDate < :endDate))";
		queryText += (isOwner) ? "" : " and a.isPrivate = :isPrivate";

		Query query = entityManager.createQuery(queryText);
		query.setParameter("userAccount", userAccount);
		query.setParameter("startDate", startDate, TemporalType.TIMESTAMP);
		query.setParameter("endDate", endDate, TemporalType.TIMESTAMP);

		if (!isOwner) {
			query.setParameter("isPrivate", false);
		}

		List<Appointment> resultList = query.getResultList();

		return resultList;
	}
	
	
	/**
	 * Checks if appointment with given start date, end date and allocation type would conflict 
	 * with another appointment of the calendar of user with userName.
	 * @param userName Username of user, the check for conflicts should happen for.
	 * @param startDate Start date of the appointment to check.
	 * @param endDate End date of the appointment to check.
	 * @param allocationType Allocation type of the appointment to check.
	 * @param currentUser Currently logged in user.
	 * @return <i>true</i>  - if appointment with given data is in conflict with another appointment<br>
	 * 		   <i>false</i> - else
	 */
	public boolean isAppointmentInConflict(String userName, Date startDate, Date endDate, EAllocationType allocationType, String currentUser) {

		UserAccount userAccount = entityManager.find(UserAccount.class, userName);
		UserAccount currentUserAccount = entityManager.find(UserAccount.class, currentUser);

		List<Appointment> appointmentList = getAppointments(userName, true, startDate, endDate);

		if (appointmentList == null || appointmentList.size() == 0) {
			return false;
		}

		if (allocationType.equals(EAllocationType.Free)) {
			return false;
		}

		int notFree = 1;

		for (Appointment app : appointmentList) {
			if (!(app.getType() == EAllocationType.Free)) {
				notFree++;

				if (notFree >= 2) {
					if (userName.equals(currentUser)) {
						String s = "Sorry, there is a conflict with your appointment \"" + app.getTitle() + "\" at " + DateFormat.getInstance().format(app.getStartDate());
						currentUserAccount.getNotifications().add(s);

					} else {
						String s;
						if (app.isPrivate()) {
							s = "Sorry, there is a conflict with an appointment of user " + userName + ".";
						} else {
							s = "Sorry, there is a conflict with the appointment \"" + app.getTitle() + "\" of user " + userName + ".";
						}
						currentUserAccount.getNotifications().add(s);

						String p = "User " + currentUser + " tried to create an appointment with you, " + "but it was in conflict with your appointment \"" + app.getTitle() + "\".";
						userAccount.getNotifications().add(p);
					}
					entityManager.persist(currentUserAccount);
					return true;

				}
			}
		}
		return false;
	}
}