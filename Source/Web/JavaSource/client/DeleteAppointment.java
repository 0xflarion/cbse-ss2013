package client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;
import entities.Appointment;

@WebServlet("/DeleteAppointment")
public class DeleteAppointment extends HttpServlet {
       
	private static final long serialVersionUID = 8465019295359561434L;
	
	ICoreManagementBean coreBean = null;

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		coreBean = (ICoreManagementBean)request.getSession().getAttribute("coreBean");
		
		try {
			Integer appointmentId = new Integer(request.getParameter("appointmentId"));
			Appointment appointment = coreBean.getEntityManager().find(Appointment.class, appointmentId);
			

			int partnerAppointmentId = appointment.getPartnerAppointmentId();
			Appointment partnerAppointment = 
					coreBean.getEntityManager().find(Appointment.class, partnerAppointmentId);
			
			
			if (appointmentId == null || appointmentId < 0) {
				String s = "Error: Appointment not found!";
				coreBean.addNotification(coreBean.getCurrentUser().getUserName(), s);
				return;
			}
			

			if (!appointment.getUserAccount().getUserName().equals(coreBean.getCurrentUser().getUserName())) {
				String s = "You are not the owner of the appointment you tried to delete!";
				coreBean.addNotification(coreBean.getCurrentUser().getUserName(), s);
				return;
			}
			
			coreBean.deleteAppointment(appointmentId);

			if (partnerAppointment != null) {
				coreBean.deleteAppointment(partnerAppointment.getId());
			}

			response.sendRedirect("/Web/home.jsp");
		
		} catch (Exception e) {
			String s = "Error: Appointment ID was not correct!";
			coreBean.addNotification(coreBean.getCurrentUser().getUserName(), s);
		}
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}