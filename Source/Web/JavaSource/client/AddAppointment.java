package client;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;
import entities.Appointment;
import enumerations.EAllocationType;

@WebServlet("/AddAppointment")
public class AddAppointment extends HttpServlet {

	private static final long serialVersionUID = 7597125652528044477L;

	private ICoreManagementBean coreBean = null;

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		coreBean = (ICoreManagementBean) request.getSession().getAttribute("coreBean");

		// get data from fields
		String userName = coreBean.getCurrentUser().getUserName();
		String partnerUserName = request.getParameter("partnerUserName");
		String notes = request.getParameter("note");
		GregorianCalendar startDate = createStartDate(request);
		GregorianCalendar endDate = createEndDate(request);
		EAllocationType allocationType = getAllocationTypeValueFromField(request);

		String title = request.getParameter("title");
		boolean priv = new Boolean(request.getParameter("isPrivate"));

		// try to create appointment for user and partner (if existent)
		Appointment appointment = coreBean.addAppointment(userName, partnerUserName, title, priv, startDate.getTime(), endDate.getTime(), allocationType, notes);

		if (appointment != null) {
			coreBean.addNotification(userName, "Appointment created!");
		}

		response.sendRedirect("/Web/home.jsp");
	}

	
	private GregorianCalendar createStartDate(HttpServletRequest request) {

		String startDate = request.getParameter("start");

		int startDay = Integer.parseInt(startDate.substring(0, 2));
		int startMonth = Integer.parseInt(startDate.substring(3, 5)) - 1;
		int startYear = Integer.parseInt(startDate.substring(6));

		int startHour = new Integer(request.getParameter("startHour"));
		int startMinute = new Integer(request.getParameter("startMinute"));

		GregorianCalendar result = new GregorianCalendar(startYear, startMonth, startDay, startHour, startMinute);

		return result;
	}

	
	private GregorianCalendar createEndDate(HttpServletRequest request) {

		String endDate = request.getParameter("end");

		int endDay = Integer.parseInt(endDate.substring(0, 2));
		int endMonth = Integer.parseInt(endDate.substring(3, 5)) - 1;
		int endYear = Integer.parseInt(endDate.substring(6));

		int endHour = new Integer(request.getParameter("endHour"));
		int endMinute = new Integer(request.getParameter("endMinute"));

		GregorianCalendar result = new GregorianCalendar(endYear, endMonth, endDay, endHour, endMinute);

		return result;
	}

	
	private EAllocationType getAllocationTypeValueFromField(HttpServletRequest request) {
		for (EAllocationType type : coreBean.getAllocationTypes()) {
			if (type.toString().equals(request.getParameter("allocationType"))) {
				return type;
			}
		}
		return EAllocationType.Free;
	}
}