package client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;

@WebServlet("/Logout")
public class Logout extends HttpServlet {

	private static final long serialVersionUID = 1L;

	ICoreManagementBean coreBean = null;

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		coreBean = (ICoreManagementBean) request.getSession().getAttribute("coreBean");

		request.getSession().setAttribute("loggedIn", false);

		coreBean.logout();

		request.getSession().setAttribute("calendar.message", "<br><h2>Goodbye</h2>");

		response.sendRedirect("/Web/home.jsp");
	}
}