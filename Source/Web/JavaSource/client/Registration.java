package client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;

import user.IUserManagementBean;
import entities.UserAccount;
import enumerations.ERegisterNotifications;

@WebServlet("/Registration")
public class Registration extends HttpServlet {

	private static final long serialVersionUID = -925087217995058168L;

	ICoreManagementBean coreBean = null;
	IUserManagementBean userBean = null;

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	/**Calendar
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ICoreManagementBean coreBean = (ICoreManagementBean) request.getSession().getAttribute("coreBean");
		userBean = (IUserManagementBean) request.getSession().getAttribute("userBean");
		if (null == coreBean || null == userBean) {
			response.sendRedirect("/Web/home.jsp");
		}

		UserAccount userAccount = userBean.getCurrentUser();

		if (userAccount == null) {
			String userName = request.getParameter("userName");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String password = request.getParameter("password");
			String passwordRepeat = request.getParameter("passwordRepeat");
			String email = request.getParameter("email");

			ERegisterNotifications registerMessage = coreBean.register(userName, firstName, lastName, password, passwordRepeat, email);

			String message = "";

			if (registerMessage == null) {
				message = message + "<br><h2>Registration failed!</h2>";
			}

			message = registerMessage.toString();
			request.getSession().setAttribute("calendar.message", message);
		}

		request.getSession().setAttribute("loggedIn", true);

		response.sendRedirect("/Web/home.jsp");
	}
}