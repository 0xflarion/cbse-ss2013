package client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;

import user.IUserManagementBean;
import clientutility.ClientUtility;

@WebServlet("/Welcome")
public class Welcome extends HttpServlet {
	
	private static final long serialVersionUID = 2754988134743609077L;

	private ICoreManagementBean coreBean;
	private IUserManagementBean userBean;

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("Miao!");
		request.getSession().setAttribute("loggedIn", false);
		lookupBeans(request);
		response.sendRedirect("home.jsp");
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	private boolean lookupBeans(HttpServletRequest request) {
		
		coreBean = (ICoreManagementBean)request.getSession().getAttribute("coreBean");
		userBean = (IUserManagementBean)request.getSession().getAttribute("userBean");

		if (coreBean == null) {
			coreBean = ClientUtility.lookupCoreManagementBean();
			
			if (coreBean != null) {
				request.getSession().setAttribute("coreBean", coreBean);
			} else {
				request.getSession().setAttribute("calendar.message", "Unable to access the AppointmentManagementBean!");
				return false;
			}
		}
		
		if (userBean == null) {
			userBean = ClientUtility.lookupUserManagementBean();
			
			if (userBean != null) {
				request.getSession().setAttribute("userBean", userBean);
			} else {
				request.getSession().setAttribute("calendar.message", "Unable to access the UserManagementBean!");				
				return false;
			}
		}
		return true;
	}
}
