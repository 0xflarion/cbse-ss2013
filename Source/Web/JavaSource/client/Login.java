package client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.ICoreManagementBean;
import entities.UserAccount;

@WebServlet("/Login")
public class Login extends HttpServlet {

	private static final long serialVersionUID = 9080443338630882850L;

	private ICoreManagementBean coreBean = null;
	private String userMessage = "";

	
	public String getUserMessage() {
		return userMessage;
	}

	
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		coreBean = (ICoreManagementBean) request.getSession().getAttribute("coreBean");

		setUserMessage("");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");

		boolean inputValid = checkValidInput(userName, password);
		if (!inputValid) {
			request.getSession().setAttribute("calendar.message", getUserMessage());
			response.sendRedirect("/Web/home.jsp");
			return;
		}

		UserAccount userAccount = login(userName, password, request);
		if (userAccount == null) {
			request.getSession().setAttribute("calendar.message", getUserMessage());
			response.sendRedirect("/Web/home.jsp");
			return;
		}

		request.getSession().setAttribute("loggedIn", true);
		response.sendRedirect("/Web/home.jsp");
	}

	
	private UserAccount login(String userName, String password, HttpServletRequest request) {

		UserAccount userAccount = coreBean.login(userName, password);

		if (userAccount == null) {
			setUserMessage(getUserMessage() + "<br><br>" + "<h2>Invalid login data!</h2>" + "<p>Username/password combination does not exist.<br>" + "Please try again to login or register.</p>");
		}
		return userAccount;
	}

	
	private boolean checkValidInput(String userName, String password) {

		boolean userNameValid = true;
		boolean passwordValid = true;

		if (!isStringExistent(userName)) {
			userNameValid = false;
		}
		if (!isStringExistent(password)) {
			passwordValid = false;
		}

		if (!userNameValid && !passwordValid) {
			setUserMessage(getUserMessage() + "<br><br>" + "<h2>Username and password are missing!</h2>" + "<p>Please enter your username and your password.");

		} else if (!userNameValid) {
			setUserMessage(getUserMessage() + "<br><br>" + "<h2>Username is missing!</h2>" + "<p>Please enter your username.");

		} else if (!passwordValid) {
			setUserMessage(getUserMessage() + "<br><br>" + "<h2>Password is missing!</h2>" + "<p>Please enter your password.");
		}

		return (userNameValid && passwordValid);
	}

	
	private boolean isStringExistent(String s) {
		if (s == null || s.length() == 0) {
			return false;
		} else {
			return true;
		}
	}

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
