package clientutility;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;

import user.IUserManagementBean;
import core.ICoreManagementBean;

public class ClientUtility extends HttpServlet {

	private static final long serialVersionUID = 4124645637077836060L;

	private static Context initialContext;
	
	private static final String PKG_INTERFACES = "org.jboss.ejb.client.naming";
	
	
	public static Context getInitialContext() {
		if (initialContext == null) {
			Properties properties = new Properties();
			properties.put(Context.URL_PKG_PREFIXES, PKG_INTERFACES);
			
			try {
				initialContext = new InitialContext(properties);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return initialContext;
	}
	
	
	public static ICoreManagementBean lookupCoreManagementBean() {
		Context jndiContext = getInitialContext();
		try {
			return (ICoreManagementBean) jndiContext.lookup("ejb:/Web/CoreManagementBean!core.ICoreManagementBean?stateful");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public static IUserManagementBean lookupUserManagementBean() {
		Context jndiContext = getInitialContext();
		try {
			return (IUserManagementBean) jndiContext.lookup("ejb:/Web/UserManagementBean!user.IUserManagementBean?stateful");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
