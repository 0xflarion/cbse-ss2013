<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Cal2013</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel="stylesheet" type="text/css" href="css/myStyle.css"></link>

</head>
<body>

	<%
		boolean loggedIn = (Boolean) request.getSession().getAttribute("loggedIn");
		IUserManagementBean userBean = (IUserManagementBean) session.getAttribute("userBean");
		ICoreManagementBean coreBean = (ICoreManagementBean) session.getAttribute("coreBean");
		HttpSession httpSession = session;
	%>

	<div id="header">
		<%@include file="header.jspf"%>
	</div>

	<div id="left">
		<%
			if (loggedIn) {
		%>
		<%@include file="menu.jspf"%>
		<%
			}
		%>
	</div>

	<div id="center">
		<%
			if (loggedIn) {
		%>
		<%@include file="notifications.jspf"%>
		<%
			} else {
		%>
		<%@include file="login.jspf"%>
		<%
			String message = (String) httpSession.getAttribute("calendar.message");
				if (message != null) {
		%>
		<p><%=message%></p>
		<%
			}
				httpSession.setAttribute("calendar.message", "");
			}
		%>
	</div>

	<div id="footer"></div>
</body>
</html>