<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Create Appointment</title>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css"></link>
<link rel="stylesheet" type="text/css" href="css/myStyle.css"></link>
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker1").datepicker({
			dateFormat : 'dd.mm.yy',
		});
		$("#datepicker2").datepicker({
			dateFormat : 'dd.mm.yy',
		});
		$("#datepicker1").datepicker('setDate', new Date());
		$("#datepicker2").datepicker('setDate', new Date());
	});
</script>
</head>
<body>
	<div id="header">
		<%@include file="header.jspf"%>
	</div>
	<%
		IUserManagementBean userBean = (IUserManagementBean) session.getAttribute("userBean");
			ICoreManagementBean coreBean = (ICoreManagementBean) session.getAttribute("coreBean");
			HttpSession httpSession = session;
			boolean loggedIn = (Boolean) request.getSession().getAttribute("loggedIn");
			if (loggedIn) {
	%>
	<div id="left"><%@include file="menu.jspf"%></div>
	<div id="center">
		<h2>Please enter appointment details:</h2>
		<%@include file="addAppointmentForm.jspf"%>
	</div>
	<%
		} else {
	%>
	<div id="left"></div>
	<div id="center"></div>
	<div id="footer"></div>
	<%
		}
	%>
</body>
</html>