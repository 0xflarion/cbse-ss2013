<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@page import="java.util.Collections"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="javax.naming.NamingException"%>
<%@page import="java.util.List"%>
<%@page import="entities.Appointment"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="java.util.Calendar"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="css/myStyle.css"></link>


</head>
<body>

	<%
		boolean loggedIn = (Boolean) request.getSession().getAttribute("loggedIn");

			ICoreManagementBean coreBean = (ICoreManagementBean) request.getSession().getAttribute("coreBean");

			UserAccount user = null;
			if (coreBean.getCurrentUser() != null) {
				user = coreBean.getCurrentUser();
			}

			Calendar now = Calendar.getInstance();
			int day = now.get(Calendar.DAY_OF_MONTH);
			int month = now.get(Calendar.MONTH) + 1;
			int year = now.get(Calendar.YEAR);

			GregorianCalendar todayDate = new GregorianCalendar(year, month - 1, day, 0, 0);
			GregorianCalendar todayDateEnd = new GregorianCalendar(year, month - 1, day, 23, 59);
			GregorianCalendar startDate = new GregorianCalendar(1970, 0, 1);
			GregorianCalendar endDate = new GregorianCalendar(2050, 11, 31);

			List<Appointment> appointments = coreBean.getAppointments(user.getUserName(), true, startDate.getTime(), endDate.getTime());
			Collections.sort(appointments);

			List<Appointment> todayAppointments = coreBean.getAppointments(user.getUserName(), true, todayDate.getTime(), todayDateEnd.getTime());
			Collections.sort(todayAppointments);
	%>


	<div id="header">
		<%@include file="header.jspf"%>
	</div>

	<%
		if (loggedIn) {
	%>

	<div id="left">
		<%@include file="menu.jspf"%>
	</div>
	<div id="center">
		<h2>Your appointments:</h2>
		<br></br>
		<div><%@include file="myAppointmentsToday.jspf"%></div>
		<br><br>
				<div><%@include file="myAppointmentsAll.jspf"%></div>
	</div>

	<%
		} else {
	%>
	<div id="left"></div>
	<div id="center">
		<%@include file="login.jspf"%>
	</div>

	<%
		}
	%>
	<div id="footer"></div>


</body>
</html>