<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<title>Register</title>

<link rel="stylesheet" type="text/css" href="css/myStyle.css"></link>

</head>

<body>

	<div id="header">
		<%@include file="header.jspf"%>
	</div>
	<div id="left"></div>

	<div id="center">

		<h1>Please fill in this form for registration:</h1>

		<%
			String my_userName = "";
			String my_firstName = "";
			String my_surName = "";
			String my_email = "";

			String my_SubmitButton = "Create Account";
		%>

		<%@include file="newUserForm.jspf"%>
	</div>

	<div id="footer"></div>
</body>
</html>